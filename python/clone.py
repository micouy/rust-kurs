a = [["first"], ["second"], ["third"]]
b = [el for el in a]

print("before:")
print(a)
print(b)

a[0][0] = "fist"

print("\nafter:")
print(a)
print(b)
