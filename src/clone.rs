fn main() {
    let mut a = [["first"], ["second"], ["third"]];
    let mut b = a.clone();

	println!("before:");
	println!("{a:?}");
	println!("{b:?}");

	a[0][0] = "fist";

	println!("\nafter:");
	println!("{a:?}");
	println!("{b:?}");
}
